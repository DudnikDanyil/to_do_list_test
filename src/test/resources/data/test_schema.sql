DROP TABLE IF EXISTS users cascade;
DROP TABLE IF EXISTS roles cascade;
DROP TABLE IF EXISTS user_roles cascade;
DROP TABLE IF EXISTS tasks cascade;


CREATE TABLE IF NOT EXISTS users
(
    id         SERIAL PRIMARY KEY,
    username   VARCHAR(255) NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name  VARCHAR(255) NOT NULL,
    email      VARCHAR(255) NOT NULL,
    password   VARCHAR(255) NOT NULL,
    status     VARCHAR(25) DEFAULT 'ACTIVE'
);

CREATE TABLE IF NOT EXISTS roles
(
    id     SERIAL PRIMARY KEY,
    name   VARCHAR(255),
    status VARCHAR(25) DEFAULT 'ACTIVE'
);


CREATE TABLE IF NOT EXISTS user_roles
(
    user_id INT REFERENCES users (id),
    role_id INT REFERENCES roles (id)
);

CREATE TABLE IF NOT EXISTS tasks
(
    id               SERIAL PRIMARY KEY,
    user_id          INTEGER,
    name_task        VARCHAR(100),
    description_tack VARCHAR(1000),
    status           VARCHAR(25) DEFAULT 'ACTIVE',
    FOREIGN KEY (user_id) REFERENCES users (id)
);

ALTER TABLE tasks ALTER COLUMN id RESTART WITH 5;
