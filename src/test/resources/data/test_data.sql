INSERT INTO users (username, first_name, last_name, email, password, status)
VALUES ('user1', 'user1', 'user1', 'email@example.com', '$2a$10$8nBAMfziop8Crze9H7b8RuafvAGt/wBDk9m9AfbRbGT7POJ6I3.ai', 'ACTIVE');

INSERT INTO users (username, first_name, last_name, email, password, status)
VALUES ('user2', 'user2', 'user2', 'email@example.com', '$2a$10$8nBAMfziop8Crze9H7b8RuafvAGt/wBDk9m9AfbRbGT7POJ6I3.ai', 'ACTIVE');


INSERT INTO roles (id, name, status)
VALUES (1, 'ROLE_USER', 'ACTIVE');

INSERT INTO roles (id, name, status)
VALUES (2, 'ROLE_NOT_USER', 'ACTIVE');

INSERT INTO user_roles (user_id, role_id)
VALUES (1, 1);

INSERT INTO user_roles (user_id, role_id)
VALUES (2, 2);


INSERT INTO tasks (id, user_id, name_task, description_tack, status)
VALUES (1, 1, 'Name task one', 'Example task one description', 'ACTIVE');

INSERT INTO tasks (id, user_id, name_task, description_tack, status)
VALUES (2, 1, 'Name task second', 'Example task second description', 'ACTIVE');

INSERT INTO tasks (id, user_id, name_task, description_tack, status)
VALUES (3, 2, 'Name task one', 'Example task one description', 'ACTIVE');

INSERT INTO tasks (id, user_id, name_task, description_tack, status)
VALUES (4, 2, 'Name task second', 'Example task second description', 'ACTIVE');