package rest.ToDoListApp.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import rest.ToDoListApp.controllers.data.TaskControllerData;
import rest.ToDoListApp.dto.task.TaskDtoOutput;
import rest.ToDoListApp.model.task.Task;
import rest.ToDoListApp.repositories.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Sql(scripts = {"classpath:/data/test_schema.sql", "classpath:/data/test_data.sql"})
@ActiveProfiles("test")
public class TaskControllerTest extends TaskControllerData {

    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void getTaskById_Test() throws Exception {

        MvcResult result = mockMvc.perform(get(GET_TASK_BY_ID_URL + TASK_ID_1)
                        .header(HEADER_KEY, HEADER_VALUE + generateToken()))
                .andExpect(status().isOk())
                .andReturn();

        String content = result.getResponse().getContentAsString();

        TaskDtoOutput actualTask = objectMapper.readValue(content, new TypeReference<>() {
        });

        assertEquals(generatedTaskDtoOutputId_1(), actualTask);

    }

    @Test
    void getTaskById_Unauthorized_Test() throws Exception {

        mockMvc.perform(get(GET_TASK_BY_ID_URL + TASK_ID_1)
                .header(HEADER_KEY, HEADER_VALUE + generateToken_Unauthorized()))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    void getAllTaskById_Test() throws Exception {

        List<TaskDtoOutput> taskDtoOutputList = new ArrayList<>();

        taskDtoOutputList.add(generatedTaskDtoOutputId_1());
        taskDtoOutputList.add(generatedTaskDtoOutputId_2());

        MvcResult result = mockMvc.perform(get(GET_ALL_TASK)
                        .header(HEADER_KEY, HEADER_VALUE + generateToken()))
                .andExpect(status().isOk())
                .andReturn();

        String content = result.getResponse().getContentAsString();

        List<TaskDtoOutput> actualTaskList = objectMapper.readValue(content, new TypeReference<>() {
        });

        assertEquals(taskDtoOutputList, actualTaskList);

    }

    @Test
    void getAllTaskById_Unauthorized_Test() throws Exception {

        mockMvc.perform(get(GET_ALL_TASK)
                        .header(HEADER_KEY, HEADER_VALUE + generateToken_Unauthorized()))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    void createTask_Test() throws Exception {

        MvcResult result = mockMvc.perform(post(CREATE_NEW_TASK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generatedTaskDtoInput()))
                        .header(HEADER_KEY, HEADER_VALUE + generateToken()))
                .andExpect(status().isOk())
                .andReturn();

        String content = result.getResponse().getContentAsString();

        TaskDtoOutput actualTask = objectMapper.readValue(content, new TypeReference<>() {
        });

        assertEquals(generatedTaskDtoOutputId_5(), actualTask);

    }

    @Test
    void createTask_Unauthorized_Test() throws Exception {

        mockMvc.perform(post(CREATE_NEW_TASK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generatedTaskDtoInput()))
                        .header(HEADER_KEY, HEADER_VALUE + generateToken_Unauthorized()))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    void updateTask_Test() throws Exception {

        MvcResult result = mockMvc.perform(put(UPDATE_TASK + TASK_ID_2)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generatedTaskDtoInput_Update()))
                        .header(HEADER_KEY, HEADER_VALUE + generateToken()))
                .andExpect(status().isOk())
                .andReturn();

        String content = result.getResponse().getContentAsString();

        TaskDtoOutput actualTask = objectMapper.readValue(content, new TypeReference<>() {
        });

        assertEquals(generatedTaskDtoOutput_Update(), actualTask);

    }

    @Test
    void updateTask_Unauthorized_Test() throws Exception {

        mockMvc.perform(put(UPDATE_TASK + TASK_ID_2)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generatedTaskDtoInput_Update()))
                        .header(HEADER_KEY, HEADER_VALUE + generateToken_Unauthorized()))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    void deleteTask_Test() throws Exception {

        mockMvc.perform(delete(DELETE_NEW_TASK + TASK_ID_2)
                        .header(HEADER_KEY, HEADER_VALUE + generateToken()))
                .andExpect(status().isOk())
                .andReturn();

        Optional<Task> deletedTask = taskRepository.findByIdAndUserId(2, 1);
        assertFalse(deletedTask.isPresent());

    }

    @Test
    void deleteTask_Unauthorized_Test() throws Exception {

        mockMvc.perform(delete(DELETE_NEW_TASK + TASK_ID_2)
                        .header(HEADER_KEY, HEADER_VALUE + generateToken_Unauthorized()))
                .andExpect(status().isForbidden())
                .andReturn();
    }
}
