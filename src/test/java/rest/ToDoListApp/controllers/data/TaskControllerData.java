package rest.ToDoListApp.controllers.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import rest.ToDoListApp.dto.task.TaskDtoInput;
import rest.ToDoListApp.dto.task.TaskDtoOutput;
import rest.ToDoListApp.model.user.Role;
import rest.ToDoListApp.repositories.TaskRepository;
import rest.ToDoListApp.security.jwt.JwtTokenProvider;

import java.util.ArrayList;
import java.util.List;

public class TaskControllerData {
    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    public static final String GET_TASK_BY_ID_URL =
            "/api/task/";
    public static final String GET_ALL_TASK =
            "/api/task/all";
    public static final String CREATE_NEW_TASK =
            "/api/task/create";
    public static final String DELETE_NEW_TASK =
            "/api/task/delete/";

    public static final String UPDATE_TASK =
            "/api/task/update/";
    public static final Integer TASK_ID_1 = 1;
    public static final Integer TASK_ID_2 = 2;

    public static final String HEADER_KEY = "Authorization";

    public static final String HEADER_VALUE = "Bearer_";

    protected String generateToken() {
        List<Role> roles = new ArrayList<>();
        Role role = new Role();
        role.setName("ROLE_USER");
        roles.add(role);
        return jwtTokenProvider.createToken("user1", roles, 1);
    }

    protected String generateToken_Unauthorized() {
        List<Role> roles = new ArrayList<>();
        Role role = new Role();
        role.setName("ROLE_NOT_USER");
        roles.add(role);
        return jwtTokenProvider.createToken("user2", roles, 2);
    }
    protected TaskDtoOutput generatedTaskDtoOutputId_1(){
        TaskDtoOutput taskDtoOutput = new TaskDtoOutput();
        taskDtoOutput.setId(1);
        taskDtoOutput.setNameTask("Name task one");
        taskDtoOutput.setDescriptionTask("Example task one description");
        taskDtoOutput.setUserId(1);

        return taskDtoOutput;
    }

    protected TaskDtoOutput generatedTaskDtoOutputId_2(){
        TaskDtoOutput taskDtoOutput = new TaskDtoOutput();
        taskDtoOutput.setId(2);
        taskDtoOutput.setNameTask("Name task second");
        taskDtoOutput.setDescriptionTask("Example task second description");
        taskDtoOutput.setUserId(1);

        return taskDtoOutput;
    }

    protected TaskDtoOutput generatedTaskDtoOutputId_5(){
        TaskDtoOutput taskDtoOutput = new TaskDtoOutput();
        taskDtoOutput.setId(5);
        taskDtoOutput.setNameTask("Name task");
        taskDtoOutput.setDescriptionTask("Example task one description");
        taskDtoOutput.setUserId(1);

        return taskDtoOutput;
    }

    protected TaskDtoInput generatedTaskDtoInput(){
        TaskDtoInput taskDtoInput = new TaskDtoInput();
        taskDtoInput.setNameTask("Name task");
        taskDtoInput.setDescriptionTask("Example task one description");

        return taskDtoInput;
    }

    protected TaskDtoInput generatedTaskDtoInput_Update(){
        TaskDtoInput taskDtoInput = new TaskDtoInput();
        taskDtoInput.setNameTask("Name task update");
        taskDtoInput.setDescriptionTask("Example task one description update");

        return taskDtoInput;
    }

    protected TaskDtoOutput generatedTaskDtoOutput_Update(){
        TaskDtoOutput taskDtoOutput = new TaskDtoOutput();
        taskDtoOutput.setId(2);
        taskDtoOutput.setNameTask("Name task update");
        taskDtoOutput.setDescriptionTask("Example task one description update");
        taskDtoOutput.setUserId(1);

        return taskDtoOutput;
    }

}
