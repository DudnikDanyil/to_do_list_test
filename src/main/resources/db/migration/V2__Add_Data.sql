
INSERT INTO users (id, username, first_name, last_name, email, password, status)
VALUES (1, 'user1', 'user1', 'user1', 'email@example.com', '$2a$10$8nBAMfziop8Crze9H7b8RuafvAGt/wBDk9m9AfbRbGT7POJ6I3.ai', 'ACTIVE');

INSERT INTO users (id, username, first_name, last_name, email, password, status)
VALUES (2, 'user2', 'user2', 'user2', 'email@example.com', '$2a$10$8nBAMfziop8Crze9H7b8RuafvAGt/wBDk9m9AfbRbGT7POJ6I3.ai', 'ACTIVE');


INSERT INTO roles (id, name, status)
VALUES (1, 'ROLE_USER', 'ACTIVE');

INSERT INTO roles (id, name, status)
VALUES (2, 'ROLE_NOT_USER', 'ACTIVE');

INSERT INTO user_roles (user_id, role_id)
VALUES (1, 1);

INSERT INTO user_roles (user_id, role_id)
VALUES (2, 2);
