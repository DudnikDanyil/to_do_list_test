INSERT INTO tasks (id, user_id, name_task, description_tack, status)
VALUES (1, 1, 'Name task one', 'Example task one description', 'ACTIVE');

INSERT INTO tasks (id, user_id, name_task, description_tack, status)
VALUES (2, 1, 'Name task second', 'Example task second description', 'ACTIVE');

INSERT INTO tasks (id, user_id, name_task, description_tack, status)
VALUES (3, 2, 'Name task one', 'Example task one description', 'ACTIVE');

INSERT INTO tasks (id, user_id, name_task, description_tack, status)
VALUES (4, 2, 'Name task second', 'Example task second description', 'ACTIVE');
