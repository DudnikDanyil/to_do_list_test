CREATE TABLE IF NOT EXISTS tasks (
                       id SERIAL PRIMARY KEY,
                       user_id INTEGER,
                       name_task VARCHAR(100),
                       description_tack VARCHAR(1000),
                       status VARCHAR(25) DEFAULT 'ACTIVE',
                       FOREIGN KEY (user_id) REFERENCES users(id)
);

