CREATE TABLE IF NOT EXISTS users (
                       id SERIAL PRIMARY KEY,
                       username VARCHAR(255) NOT NULL,
                       first_name VARCHAR(255) NOT NULL,
                       last_name VARCHAR(255) NOT NULL,
                       email VARCHAR(255) NOT NULL,
                       password VARCHAR(255) NOT NULL,
                       status VARCHAR(25) DEFAULT 'ACTIVE'
);

CREATE TABLE IF NOT EXISTS roles (
                       id SERIAL PRIMARY KEY,
                       name VARCHAR(255),
                       status VARCHAR(25) DEFAULT 'ACTIVE'
);


CREATE TABLE IF NOT EXISTS user_roles (
                            user_id INT REFERENCES users(id),
                            role_id INT REFERENCES roles(id)
);