package rest.ToDoListApp.services.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rest.ToDoListApp.dto.user.AdminUserDto;
import rest.ToDoListApp.dto.user.UserDtoInput;
import rest.ToDoListApp.dto.user.UserDtoOutput;
import rest.ToDoListApp.dto.task.TaskDtoInput;
import rest.ToDoListApp.dto.task.TaskDtoOutput;
import rest.ToDoListApp.model.task.Task;
import rest.ToDoListApp.model.user.User;

import java.util.ArrayList;
import java.util.List;

@Service
public class MapperService {

    private final ModelMapper modelMapper;

    @Autowired
    public MapperService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public User convertToUser(UserDtoInput userDtoInput) {
        return modelMapper.map(userDtoInput, User.class);
    }

    public UserDtoOutput convertToUserDto(User user) {
        return modelMapper.map(user, UserDtoOutput.class);
    }

    public AdminUserDto convertToAdminUserDto(User user) {
        return modelMapper.map(user, AdminUserDto.class);
    }

    public Task convertToTask(TaskDtoInput taskDtoInput){
        return modelMapper.map(taskDtoInput, Task.class);
    }

    public TaskDtoOutput convertToTaskDtoOutput(Task task){
        return modelMapper.map(task, TaskDtoOutput.class);
    }

    public List<TaskDtoOutput> convertToTaskDtoOutputList(List<Task> tasksList) {

        List<TaskDtoOutput> taskDtoOutputDTOList = new ArrayList<>();
        for (Task task : tasksList) {
            taskDtoOutputDTOList.add(convertToTaskDtoOutput(task));
        }
        return taskDtoOutputDTOList;
    }
}
