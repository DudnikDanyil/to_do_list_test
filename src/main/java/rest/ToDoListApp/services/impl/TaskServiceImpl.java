package rest.ToDoListApp.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rest.ToDoListApp.model.emuns.Status;
import rest.ToDoListApp.model.task.Task;
import rest.ToDoListApp.repositories.TaskRepository;
import rest.ToDoListApp.security.jwt.JwtTokenProvider;
import rest.ToDoListApp.security.jwt.JwtUser;
import rest.ToDoListApp.services.TaskService;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;
    private final JwtTokenProvider jwtTokenProvider;

    @Autowired
    public TaskServiceImpl(TaskRepository taskRepository, JwtTokenProvider jwtTokenProvider) {
        this.taskRepository = taskRepository;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    @Transactional
    public Task save(Task task) {

        task.setUserId(getUserIdFromCurrentSession());
        task.setStatus(Status.ACTIVE);

        taskRepository.save(task);
        return task;
    }

    @Override
    public List<Task> getAll() {

        return taskRepository.findAllByUserId(getUserIdFromCurrentSession());
    }

    @Override
    public Task getById(int taskId) {
        Optional<Task> task = taskRepository.findByIdAndUserId(taskId, getUserIdFromCurrentSession());

        if (task.isEmpty()) {
            throw new EntityNotFoundException("No task with ID " + taskId + " found");
        }

        return task.get();
    }

    @Override
    @Transactional
    public Task update(int taskId, Task taskUpdate) {

        if (taskUpdate == null) {
            throw new IllegalArgumentException("Task update data is null. You must provide valid data to update the task.");
        }

        Optional<Task> task = taskRepository.findByIdAndUserId(taskId, getUserIdFromCurrentSession());
        if (task.isEmpty()) {
            throw new EntityNotFoundException("No task with ID " + taskId + " found");
        }

        Task existingTask = task.get();
        existingTask.setNameTask(taskUpdate.getNameTask());
        existingTask.setDescriptionTask(taskUpdate.getDescriptionTask());

        return taskRepository.save(existingTask);
    }

    @Override
    @Transactional
    public void delete(int taskId) {

        Optional<Task> task = taskRepository.findByIdAndUserId(taskId, getUserIdFromCurrentSession());

        if (task.isEmpty()) {
            throw new EntityNotFoundException("No task with ID " + taskId + " found");
        }

        taskRepository.deleteByIdAndUserId(taskId, getUserIdFromCurrentSession());
    }


    private int getUserIdFromCurrentSession() {
        JwtUser jwtUser = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return jwtUser.getId();
    }
}
