package rest.ToDoListApp.services;

import org.springframework.security.access.prepost.PreAuthorize;
import rest.ToDoListApp.model.task.Task;

import java.util.List;

public interface TaskService {

    @PreAuthorize("hasRole('USER')")
    Task save(Task task);

    @PreAuthorize("hasRole('USER')")
    List<Task> getAll();

    @PreAuthorize("hasRole('USER')")
    Task getById(int taskId);

    @PreAuthorize("hasRole('USER')")
    Task update(int taskId, Task task);

    @PreAuthorize("hasRole('USER')")
    void delete(int taskId);
}
