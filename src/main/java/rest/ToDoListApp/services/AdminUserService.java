package rest.ToDoListApp.services;

import rest.ToDoListApp.model.user.User;

public interface AdminUserService {

    User registerAdmin(User user);
}
