package rest.ToDoListApp.dto.task;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class TaskDtoOutput {

    int id;
    @Schema(description = "Name task", example = "task number one")
    String nameTask;
    @Schema(description = "Description task", example = "example task description")
    String descriptionTask;
    @Schema(description = "User's id", example = "1")
    int userId;
}
