package rest.ToDoListApp.dto.task;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class TaskDtoInput {

    @NotNull
    @Size(min = 5, max = 100)
    @Schema(description = "Name task", example = "task number one")
    String nameTask;

    @NotNull
    @Size(min = 10, max = 1000)
    @Schema(description = "Description task", example = "example task description")
    String descriptionTask;
}
