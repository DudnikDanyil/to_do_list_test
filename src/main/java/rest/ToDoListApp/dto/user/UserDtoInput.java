package rest.ToDoListApp.dto.user;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserDtoInput {

    @NotNull
    @Size(min = 2, max = 100)
    @Schema(description = "User's username", example = "user1")
    private String username;

    @NotNull
    @Size(min = 2, max = 100)
    @Schema(description = "User's first name", example = "user1")
    private String firstName;

    @NotNull
    @Size(min = 2, max = 100)
    @Schema(description = "User's last name", example = "user1")
    private String lastName;

    @NotNull
    @Email
    @Schema(description = "User's email", example = "email@example.com")
    private String email;

    @NotNull
    @Size(min = 10, max = 100)
    @Schema(description = "User's password", example = "969696")
    private String password;

}
