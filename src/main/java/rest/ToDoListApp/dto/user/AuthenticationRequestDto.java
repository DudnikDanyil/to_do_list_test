package rest.ToDoListApp.dto.user;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class AuthenticationRequestDto {

    @Schema(description = "User's username", example = "user1")
    private String username;

    @Schema(description = "User's password", example = "969696")
    private String password;
}