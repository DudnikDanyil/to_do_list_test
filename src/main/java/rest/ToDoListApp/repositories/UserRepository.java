package rest.ToDoListApp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rest.ToDoListApp.model.user.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsername(String username);
    boolean existsByUsername(String username);
    boolean existsByEmail(String username);
    void deleteById(Integer id);
}
