package rest.ToDoListApp.exceptions;

public class SuperAdminDeletionException extends RuntimeException {

    public SuperAdminDeletionException(String message) {
        super(message);
    }
}
