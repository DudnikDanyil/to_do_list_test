package rest.ToDoListApp.exceptions.exceptionHandlers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import rest.ToDoListApp.exceptions.SuperAdminDeletionException;
import rest.ToDoListApp.exceptions.UserAlreadyExistsException;
import rest.ToDoListApp.model.ApiError;

import javax.persistence.EntityNotFoundException;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(SuperAdminDeletionException.class)
    public ResponseEntity<Object> handleSuperAdminDeletionException(SuperAdminDeletionException ex, WebRequest request) {
        String message = ex.getMessage();
        ApiError apiError = new ApiError(HttpStatus.FORBIDDEN, message, request.getDescription(false));
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    public ResponseEntity<Object> handleUserAlreadyExistsException(UserAlreadyExistsException ex, WebRequest request) {
        String message = ex.getMessage();
        ApiError apiError = new ApiError(HttpStatus.CONFLICT, message, request.getDescription(false));
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Object> handleEntityNotFoundException(EntityNotFoundException ex, WebRequest request) {
        String message = ex.getMessage();
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, message, request.getDescription(false));
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }
}
