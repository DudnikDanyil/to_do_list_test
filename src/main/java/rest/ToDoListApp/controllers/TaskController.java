package rest.ToDoListApp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rest.ToDoListApp.dto.task.TaskDtoInput;
import rest.ToDoListApp.dto.task.TaskDtoOutput;
import rest.ToDoListApp.services.TaskService;
import rest.ToDoListApp.services.mapper.MapperService;
import rest.ToDoListApp.services.impl.TaskServiceImpl;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/task")
public class TaskController {

    private final MapperService mapperService;

    private final TaskService taskService;

    @Autowired
    public TaskController(MapperService mapperService, TaskServiceImpl taskService) {
        this.mapperService = mapperService;
        this.taskService = taskService;
    }

    @PostMapping("/create")
    public ResponseEntity<TaskDtoOutput> create(@Valid @RequestBody TaskDtoInput taskDtoInput) {

        TaskDtoOutput taskDtoOutput = mapperService.convertToTaskDtoOutput(
                taskService.save(mapperService.convertToTask(taskDtoInput)));

        return  ResponseEntity.ok(taskDtoOutput);
    }

    @GetMapping("/all")
    public ResponseEntity<List<TaskDtoOutput>> getAllTasks() {

        List<TaskDtoOutput> taskDtoOutputList = mapperService.convertToTaskDtoOutputList(
                taskService.getAll());

        return  ResponseEntity.ok(taskDtoOutputList);
    }

    @GetMapping("/{taskId}")
    public ResponseEntity<TaskDtoOutput> getTaskById(@PathVariable("taskId") int taskId){

        TaskDtoOutput taskDtoOutput = mapperService.convertToTaskDtoOutput(
                taskService.getById(taskId));

        return ResponseEntity.ok(taskDtoOutput);
    }

    @PutMapping("/update/{taskId}")
    public ResponseEntity<TaskDtoOutput> updateTask(@PathVariable("taskId") int taskId,
                                                  @RequestBody TaskDtoInput taskDtoInput){

        TaskDtoOutput taskDtoOutput = mapperService.convertToTaskDtoOutput(
                taskService.update(taskId, mapperService.convertToTask(taskDtoInput)));

        return ResponseEntity.ok(taskDtoOutput);
    }

    @DeleteMapping("/delete/{taskId}")
    public HttpStatus deleteTask(@PathVariable("taskId") int taskId){

        taskService.delete(taskId);

        return HttpStatus.OK;
    }

}
