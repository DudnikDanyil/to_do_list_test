package rest.ToDoListApp.controllers.userControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rest.ToDoListApp.dto.user.AdminUserDto;
import rest.ToDoListApp.dto.user.UserDtoInput;
import rest.ToDoListApp.dto.user.UserDtoOutput;
import rest.ToDoListApp.services.mapper.MapperService;
import rest.ToDoListApp.services.UserService;

@RestController
@RequestMapping(value = "/api")
public class RegistrationRestController {

    private final UserService userService;
    private final MapperService mapperService;

    @Autowired
    public RegistrationRestController(UserService userService,
                                      MapperService mapperService) {
        this.userService = userService;
        this.mapperService = mapperService;
    }

    @PostMapping(value = "/registr")
    public ResponseEntity<UserDtoOutput> registrationUser(@RequestBody UserDtoInput userDtoInput) {

        UserDtoOutput userDtoOutput = mapperService.convertToUserDto(userService
                .register(mapperService.convertToUser(userDtoInput)));

        return ResponseEntity.ok(userDtoOutput);
    }

    @PostMapping(value = "/registr/admin")
    public ResponseEntity<AdminUserDto> registrationUserAdmin(@RequestBody UserDtoInput userDtoInput) {

        AdminUserDto adminUserDto = mapperService.convertToAdminUserDto(userService
                .register(mapperService.convertToUser(userDtoInput)));

        return ResponseEntity.ok(adminUserDto);
    }

}
