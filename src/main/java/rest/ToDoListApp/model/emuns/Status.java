package rest.ToDoListApp.model.emuns;

public enum Status {
    ACTIVE, NOT_ACTIVE, DELETED
}
