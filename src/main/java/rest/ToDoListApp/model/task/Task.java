package rest.ToDoListApp.model.task;

import lombok.Data;
import rest.ToDoListApp.model.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tasks")
@Data
public class Task extends BaseEntity {

    @Column(name = "name_task")
    String nameTask;

    @Column(name = "description_tack")
    String descriptionTask;

    @Column(name = "user_id")
    int userId;
}
